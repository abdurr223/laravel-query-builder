<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/cast', function () {
//     return view('cast');
// });
// Route::get('/cast', [app\Http\Controllers\CastController::class,'index']);
Route::get('/cast', [App\Http\Controllers\CastController::class,'Index']);
Route::get('/cast/create', [App\Http\Controllers\CastController::class,'Create']);
Route::post('/cast', [App\Http\Controllers\CastController::class,'Store']);
Route::get('/cast/edit/{id}', [App\Http\Controllers\CastController::class,'Edit']);
Route::patch('/cast/{id}', [App\Http\Controllers\CastController::class,'editProcces']);
Route::delete('/cast/{id}', [App\Http\Controllers\CastController::class,'delete']);
