@extends('master.template')
@section('container')
<div class="nav justify-content-end mb-4">
<a href="/cast/create" class="btn btn-outline-success">Tambah Data</a>
</div>
<table class="table table-striped">
    @if (session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>        
    @endif
    <thead>
        <tr>
            <th class="text-center">No</th>
            <th class="text-center">Nama</th>
            <th class="text-center">Umur</th>
            <th class="text-center">Bio</th>
            <th class="text-center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    @foreach($cast as $item)
    <tr>
        <td class="text-center">{{$loop -> iteration}}</td>
        <td class="text-center">{{$item->nama}}</td>
        <td class="text-center">{{$item->umur}}</td>
        <td class="text-center">{{$item->bio}}</td>
        <td class="text-center">
            <a href="{{url('cast/edit/'.$item->id) }}" class="btn btn-primary btn-sm">
                <i class="fa fa-pencil"></i>
            </a>
            <form action="{{url('cast/'.$item->id)}}" class="d-inline" method="post" onsubmit="return confirm('Yakin Hapus Data?')">
                @method('delete')
                @csrf
                <button class="btn btn-danger btn-sm">
                    <i class="fa fa-trash"></i>
                </button>

            </form>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
@endsection